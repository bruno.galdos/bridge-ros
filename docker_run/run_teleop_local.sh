#!/bin/bash

##################################################
# Configuration
##################################################

ROS_DOMAIN_ID=30

#ROBOT_PREFIX="kairosAB"
ROBOT_PREFIX="kairosAA"
# Name of docker containers and docker images
CONTAINER_NAME_TELEOP="${ROBOT_PREFIX}-teleop"
IMAGE_NAME_TELEOP="osrf/ros:humble-desktop"

##################################################
# Functions definitions
##################################################

# Docker kill commands
DOCKER_KILL_COMMAND_TELEOP="docker ps -q --filter name=${CONTAINER_NAME_TELEOP} | grep -q . && docker rm -fv ${CONTAINER_NAME_TELEOP}"

# Function to kill all docker containers with q
function kill_all() {
	bash -c "${DOCKER_KILL_COMMAND_TELEOP}"

	# Kill tmux
	sleep 1
	tmux kill-session -t $(tmux display-message -p '#S')
}

# Function that waits for user input and then kills all running docker containers and tmux session
function wait_for_user_and_kill() {
    echo "Press 'q' to kill every process"
    while :; do
        read -n 1 key <&1

        if [[ ${key} == "q" ]]; then
            printf "\nQuitting all processes\n"
            kill_all
        fi
    done
}

# Check user input
if [ "$1" == "wait_for_user_and_kill" ]; then
    wait_for_user_and_kill
    exit 0
fi

##################################################
# Tmux session
##################################################

# Create a new session named ${TMUX_SESSION_NAME}, split panes and change directory in each
TMUX_SESSION_NAME=${ROBOT_PREFIX}-teleop
tmux new-session -d -s ${TMUX_SESSION_NAME}

# Config tmux
tmux set -g mouse on

# Run teleop in ROS2
tmux send-keys -t ${TMUX_SESSION_NAME} \
    "${DOCKER_KILL_COMMAND_TELEOP}; \
    docker run \
        -it \
        --rm \
        --net=host \
        --env ROS_DOMAIN_ID=${ROS_DOMAIN_ID} \
        --name ${CONTAINER_NAME_TELEOP} \
        ${IMAGE_NAME_TELEOP} \
        ros2 run teleop_twist_keyboard teleop_twist_keyboard \
        --ros-args --remap __node:=${ROBOT_PREFIX}_teleop \
        --remap /cmd_vel:=/${ROBOT_PREFIX}/cmd_vel" \
    Enter

# Kill process terminal
tmux split-window -hf -p 33 -t ${TMUX_SESSION_NAME}
tmux send-keys -t ${TMUX_SESSION_NAME} "bash $0 wait_for_user_and_kill" Enter

# Attach to session named ${TMUX_SESSION_NAME}
tmux attach -t ${TMUX_SESSION_NAME}
