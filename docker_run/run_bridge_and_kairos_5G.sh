#!/bin/bash

##################################################
# Configuration
##################################################

# IP and name of robot
ROBOT_IP="172.16.35.17"
ROBOT_NAME="kairos"
ROBOT_PREFIX="kairosAB"

ROS_DOMAIN_ID=42
ROS_MASTER_URI="http://${ROBOT_IP}:11311"
RMW_IMPLEMENTATION=rmw_cyclonedds_cpp

# Name of docker containers and docker images
CONTAINER_NAME_ROSCORE="roscore"
CONTAINER_NAME_ROBOT_DRIVER="kairos-ros"
CONTAINER_NAME_BRIDGE="${ROBOT_PREFIX}-ros1-bridge"

IMAGE_NAME_ROSCORE="hades:5000/kairos-ros:kinetic"
IMAGE_NAME_ROBOT_DRIVER="hades:5000/kairos-ros:kinetic"
IMAGE_NAME_BRIDGE="hades:5000/bridge-ros:main"

##################################################
# Functions definitions
##################################################

# Docker kill commands
DOCKER_KILL_COMMAND_ROSCORE="docker ps -q --filter name=${CONTAINER_NAME_ROSCORE} | grep -q . && docker rm -fv ${CONTAINER_NAME_ROSCORE}"
DOCKER_KILL_COMMAND_ROBOT_DRIVER="docker ps -q --filter name=${CONTAINER_NAME_ROBOT_DRIVER} | grep -q . && docker rm -fv ${CONTAINER_NAME_ROBOT_DRIVER}"
DOCKER_KILL_COMMAND_BRIDGE="docker ps -q --filter name=${CONTAINER_NAME_BRIDGE} | grep -q . && docker rm -fv ${CONTAINER_NAME_BRIDGE}"

# Function to test the connection
function ping_test() {
    IP="$1"

    if ping -c 1 ${IP} &>/dev/null; then
        return 0
    else
        return 1
    fi
}

# Function to kill all docker containers with q
function kill_all() {
	bash -c "ssh ${ROBOT_NAME}@${ROBOT_IP} \
		'${DOCKER_KILL_COMMAND_ROBOT_DRIVER};
		 ${DOCKER_KILL_COMMAND_BRIDGE};
		 ${DOCKER_KILL_COMMAND_ROSCORE}'"

	# Kill tmux
	sleep 5
	tmux kill-session -t $(tmux display-message -p '#S')
}

# Function that waits for user input and then kills all running docker containers and tmux session
function wait_for_user_and_kill() {
    echo "Press 'q' to kill every process"
    while :; do
        read -n 1 key <&1

        if [[ ${key} == "q" ]]; then
            printf "\nQuitting all processes\n"
            kill_all
        fi
    done
}

# Check user input
if [ "$1" == "wait_for_user_and_kill" ]; then
    wait_for_user_and_kill
    exit 0
fi

##################################################
# Tmux session
##################################################

ping_test ${ROBOT_IP}
if [ $? -eq 1 ]; then
	echo "Could not connect to robot"
	exit 1
fi

# Create a new session named ${TMUX_SESSION_NAME}, split panes and change directory in each
TMUX_SESSION_NAME=${ROBOT_PREFIX}-bridge
tmux new-session -d -s ${TMUX_SESSION_NAME}

# Config tmux
tmux set -g mouse on

# Run roscore
tmux send-keys -t ${TMUX_SESSION_NAME} \
    "ssh -t ${ROBOT_NAME}@${ROBOT_IP} \
    '${DOCKER_KILL_COMMAND_ROSCORE}; \
    docker run \
        -it \
        --rm \
        --net=host \
        --env ROS_MASTER_URI=${ROS_MASTER_URI} \
        --env ROS_IP=${ROBOT_IP} \
        --name ${CONTAINER_NAME_ROSCORE} \
        ${IMAGE_NAME_ROSCORE} \
        roscore'" \
    Enter

sleep 2

# Run bridge
tmux split-window -t ${TMUX_SESSION_NAME}
tmux send-keys -t ${TMUX_SESSION_NAME} \
    "ssh -t ${ROBOT_NAME}@${ROBOT_IP} \
    '${DOCKER_KILL_COMMAND_BRIDGE}; \
    docker run \
        -it \
        --rm \
        --net=host \
        --ipc=host \
        --pid=host \
        --env ROS_MASTER_URI=${ROS_MASTER_URI} \
        --env ROS_IP=${ROBOT_IP} \
        --env ROS_DOMAIN_ID=${ROS_DOMAIN_ID} \
        --env RMW_IMPLEMENTATION=${RMW_IMPLEMENTATION} \
        --env ROBOT_PREFIX=${ROBOT_PREFIX} \
        --name ${CONTAINER_NAME_BRIDGE} \
        ${IMAGE_NAME_BRIDGE} \
        ros2 run ros1_bridge parameter_bridge \
			__name:=${ROBOT_PREFIX}_ros1_bridge \
			--ros-args -r __node:=${ROBOT_PREFIX}_ros1_bridge'" \
    Enter

sleep 2

# SSH onto kairos, remove running docker container and run docker container "kairos-ros"
tmux split-window -hf -t ${TMUX_SESSION_NAME}
tmux send-keys -t ${TMUX_SESSION_NAME} \
	"ssh -t ${ROBOT_NAME}@${ROBOT_IP} \
	'${DOCKER_KILL_COMMAND_ROBOT_DRIVER}; \
	docker run \
		-it \
		--rm \
		--net=host \
		--privileged \
		--volume=/dev:/dev \
		--env ROS_MASTER_URI=${ROS_MASTER_URI} \
		--env ROS_IP=${ROBOT_IP} \
		--env ROBOT_XACRO=rbkairos_ur10_egh.urdf.xacro \
		--name ${CONTAINER_NAME_ROBOT_DRIVER} \
		${IMAGE_NAME_ROBOT_DRIVER} \
		./kairos_entrypoint.sh'" \
	Enter

# Kill process terminal
tmux split-window -p 33 -t ${TMUX_SESSION_NAME}
tmux send-keys -t ${TMUX_SESSION_NAME} "bash $0 wait_for_user_and_kill" Enter

# Attach to session named ${TMUX_SESSION_NAME}
tmux attach -t ${TMUX_SESSION_NAME}
