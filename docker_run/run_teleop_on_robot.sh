#!/bin/bash

# IP addresses and names of hosts
RMW_IMPLEMENTATION=rmw_cyclonedds_cpp

HOST_NAME=kairos
HOST_IP="192.168.10.197"
ROS_DOMAIN_ID_OUSTER=30
ROBOT_PREFIX="kairosAB"
IMAGE_NAME_TELEOP="osrf/ros:noetic-teleop"

# Name of docker containers and docker images
CONTAINER_NAME_TELEOP="${ROBOT_PREFIX}-teleop"

# Docker kill commands
DOCKER_KILL_COMMAND_TELEOP="docker ps -q --filter name=${CONTAINER_NAME_TELEOP} | grep -q . && docker rm -fv ${CONTAINER_NAME_TELEOP}"

# Tmux session
TMUX_SESSION='tmux-session_'
RANDOM_NUMBER=$RANDOM
TMUX_SESSION_NAME="$TMUX_SESSION$RANDOM_NUMBER"

# Function to test the connection
function ping_test() {
	if ping -c 1 $1 &>/dev/null; then
		echo "Connection to $1 successful!"
	else
		echo "Can't establish a connection to $1!"
	fi
}

# Function to kill all docker containers with q
function kill_all() {
	bash -c "ssh ${HOST_NAME}@${HOST_IP} '${DOCKER_KILL_COMMAND_TELEOP}'"

	# Kill tmux
	sleep 5
    TMUX_SESSION_NAME_ATTACHED=$(tmux display-message -p '#S')
    tmux kill-session -t ${TMUX_SESSION_NAME_ATTACHED}
    tmux kill-session -t ${TMUX_SESSION_NAME}
}

# Function that waits for user input and then kills all running docker containers and tmux session
function wait_for_user_and_kill() {
	echo "Press 'q' to kill every process"
	while :; do
		read -n 1 k <&1

		if [[ $k = q ]]; then
			printf "\nQuitting all processes\n"
			kill_all
		fi
	done
}
"$@"

# Allow clients to access the x-server
xhost +

# Create a new session named $TMUX_SESSION_NAME, split panes and change directory in each
tmux new-session -d -s $TMUX_SESSION_NAME

# Config tmux
tmux set -g mouse on

# Run teleop
ping_test $HOST_IP
tmux send-keys -t $TMUX_SESSION_NAME "ssh -Y -t ${HOST_NAME}@${HOST_IP} \
	'${DOCKER_KILL_COMMAND_TELEOP}; \
	docker run \
        -it \
        --rm \
        --net=host \
        --env ROS_DOMAIN_ID=${ROS_DOMAIN_ID} \
        --name ${CONTAINER_NAME_TELEOP} \
        ${IMAGE_NAME_TELEOP} \
        rosrun teleop_twist_keyboard teleop_twist_keyboard.py __name:=${ROBOT_PREFIX}_teleop /cmd_vel:=/${ROBOT_PREFIX}/cmd_vel'" Enter

sleep 2

# Kill process terminal
tmux split-window -t $TMUX_SESSION_NAME
tmux send-keys -t $TMUX_SESSION_NAME "bash $0 wait_for_user_and_kill" Enter

# Attach to session named TMUX_SESSION_NAME
tmux attach -t $TMUX_SESSION_NAME
