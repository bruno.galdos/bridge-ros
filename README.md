# Bridge ROS

## Description

This repository contains ways of starting a bridge between ROS1 and ROS2 using docker, using as primary goal the creation of a bridge between ROS humble and ROS kinetic.

## Usage

The Dockerfile present at the root of this repository uses the [`ros:rolling-ros1-bridge`](https://hub.docker.com/_/ros/) image as base to create a bridge for the ROS messages, as long as the messages used in the ROS1 version are compatible with the messages from ROS noetic and the messages used in the ROS2 version are compatible with the messages from ROS rolling. To be able to use this image inside the robots, the same image is being pushed to the `hades:5000` docker registry, with the tag `hades:5000/bridge-ros:main`.

In the root of this repository there are two docker compose files which can be used to start the bridge in a container using the images described above. The `docker-compose-host.yml` uses the `registry.git-ce.rwth-aachen.de/wzl-mq-ms/docker-ros/bridge-ros:main` image, while the `docker-compose-robot.yml` uses the `hades:5000/bridge-ros:main` image, so use `docker pull` to pull the image that will be used before running the docker compose commands. Note that it is important to set some environment variables in order for the bridge to work with multiple hosts, the variables are:

- [`ROS_MASTER_URI`](http://wiki.ros.org/ROS/EnvironmentVariables#ROS_MASTER_URI) - URI of the master
- [`ROS_IP`](http://wiki.ros.org/ROS/EnvironmentVariables#ROS_IP.2FROS_HOSTNAME) - IP of your computer in the network
- [`ROS_DOMAIN_ID`](https://docs.ros.org/en/humble/Concepts/About-Domain-ID.html) - DDS domain ID of the ROS2 logical network being used

Besides that, it is important to set the `ROBOT_PREFIX` environment variable, so the bridge can know which robot is being used and which topics to bridge. The default value for this variable is `robot`, but it can be changed to any other value. For kairos use `kairosAA` or `kairosBB` depending on which kairos is being used.

One final important detail is the [RMW implementation](https://docs.ros.org/en/humble/Concepts/About-Different-Middleware-Vendors.html) that is being used. The default value is for the bridge is the `rmw_fastrtps_cpp`, but currently, due to a Fast DDS issue, in order to run the bridge in the same machine that other ROS2 nodes are being run, it is necessary to use the `rmw_cyclonedds_cpp`. To choose the RMW implementation being used, set the `RMW_IMPLEMENTATION` environment variable accordingly.

If the variables are already set correctly in the current shell, just run:

```bash
docker compose -f docker-compose-<TARGET>.yml up
```

If some value need to be changed, use the `-e` flag to set the environment variables with `docker compose run`, for example:

```bash
docker compose -f docker-compose-<TARGET>.yml run -i --rm -e ROS_MASTER_URI='http://192.168.10.20:11311' -e ROS_IP='192.168.10.20' bridge
```

Observation: To be able to bridge custom messages, it is necessary to make changes to the current setup.

## Example

An example of starting the bridge alongside demo nodes from ROS kinetic and ROS humble can be seen in the `example-compose.yml` file in the examples folder.

Besides that, two scripts were elaborated in order to test the bridge with [kairos](https://git-ce.rwth-aachen.de/wzl-mq-ms/docker-ros/kairos-ros). To run the scripts it is necessary to have `tmux` installed and to be able to ssh into kairos as described [here](https://git-ce.rwth-aachen.de/wzl-mq-ms/docker-ros/kairos-ros#ssh-access-to-robot). To run the scripts also make sure to update the `SERVER_IP` variable in the beginning of the script depending on the IP of your computer on the network and to update the `ROBOT_IP` depending on which kairos is being used. The scripts are:

- `kairos_bridge_host_test.sh` - Starts roscore, the bridge and the [teleop_twist_keyboard](https://index.ros.org/r/teleop_twist_keyboard/) node on the host and the robot driver on kairos.
- `kairos_bridge_robot_test.sh` - Starts roscore and the [teleop_twist_keyboard](https://index.ros.org/r/teleop_twist_keyboard/) node on the host and the bridge and the robot driver on kairos.
