ARG ROS2_DISTRO=rolling
ARG ROS1_DISTRO=noetic

FROM ros:${ROS2_DISTRO}-ros1-bridge as base

ARG BRIDGE_REPO=LucasHaug/ros1_bridge

ENV RMW_IMPLEMENTATION=rmw_cyclonedds_cpp

ENV BRIDGE_WS /bridge_ws

ADD https://api.github.com/repos/${BRIDGE_REPO}/git/refs/heads/master /.git-hashref

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt update && \
    apt install -y \
        git \
        ros-${ROS2_DISTRO}-control-msgs \
        ros-${ROS2_DISTRO}-rmw-fastrtps-cpp \
        ros-${ROS2_DISTRO}-rmw-cyclonedds-cpp \
        ros-${ROS1_DISTRO}-control-msgs && \
    mkdir -p $BRIDGE_WS/src && \
    cd $BRIDGE_WS/src && \
    git clone https://github.com/${BRIDGE_REPO}.git && \
    cd $BRIDGE_WS && \
    apt purge -y git ros-${ROS2_DISTRO}-ros1-bridge && \
    /ros_entrypoint.sh colcon build --symlink-install --packages-select ros1_bridge --cmake-force-configure

COPY bridge_entrypoint.sh /
COPY params /params

ENTRYPOINT [ "/bridge_entrypoint.sh" ]
